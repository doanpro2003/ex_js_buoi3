// câu1
document.getElementById('sapXep').onclick = function () {
    var num1 = document.getElementById('txt-num1').value;
    var num2 = document.getElementById('txt-num2').value;
    var num3 = document.getElementById('txt-num3').value;
    var tg;
    if (num1 > num2) {
        tg = num1;
        num1 = num2;
        num2 = tg;
    }
    if (num1 > num3) {
        tg = num1;
        num1 = num3;
        num3 = tg;
    }
    if (num2 > num3) {
        tg = num2;
        num2 = num3;
        num3 = tg;
    }
    document.getElementById('kqSapXep').innerHTML = num1 + "<" + num2 + "<" + num3;
}
// câu2
document.getElementById('submit').onclick = function () {
    var result = document.getElementById('people')
    document.getElementById('result').innerHTML = "Xin Chào " + result.options[result.selectedIndex].text;
}
// câu 3
document.getElementById('count').onclick = function () {
    var num1 = document.getElementById('txt-num-1').value;
    var num2 = document.getElementById('txt-num-2').value;
    var num3 = document.getElementById('txt-num-3').value;
    var dem = 0;
    var arr = [];
    arr.push(num1);
    arr.push(num2);
    arr.push(num3);

    for (let i = 0; i < 3; i++) {
        if (arr[i] % 2 == 0) {
            dem++
        }
    }
    document.getElementById('kqCount').innerHTML = "so chan: " + dem + " so le:" + (3 - dem)
}
// câu 4
document.getElementById('tam-giac').onclick = function () {
    var a = document.getElementById('txt-edge-1').value * 1;
    var b = document.getElementById('txt-edge-2').value * 1;
    var c = document.getElementById('txt-edge-3').value * 1;
    if (b + c > a && b + a > c && c + a > b) {
        if (a == b && b == c) {
            document.getElementById('kq-tam-giac').innerHTML = "thoa man la tam giac deu";
        }
        else if (a * a + b * b == c * c || b * b + c * c == a * a || a * a + c * c == b * b) {
            document.getElementById('kq-tam-giac').innerHTML = "thoa la tam giac vuong";
        }
        else if (a == b || b == c || a == c) {
            document.getElementById('kq-tam-giac').innerHTML = "thoa man la tam giac can";
        }
        else {
            document.getElementById('kq-tam-giac').innerHTML = "Đây là tam giác nhọn";
        }
    }
    else { document.getElementById('kq-tam-giac').innerHTML = "khong thoa man la tam giac"; }
}
// câu5

function nhuan(y) {
    return ((y % 4 == 0 && y % 100 != 0) || y % 400 == 0);
}

function songaytrongthang(m, y) {
    switch (m) {
        case 1: case 3: case 5: case 7: case 8: case 10: case 12:
            {
                return 31;
                break;
            }
        case 2:
            {
                if (nhuan(y)) {
                    return 29;
                }
                return 28;
            }
        case 4: case 6: case 9: case 11:
            {
                return 30;
            }
    }
}

document.getElementById('date-before').onclick = function () {

    var d = document.getElementById('txt-date-inf').value * 1;
    var m = document.getElementById('txt-month-inf').value * 1;
    var y = document.getElementById('txt-year-inf').value * 1;


    var nextyear = y;
    var nextmonth = m;
    var nextday = d;
    //nếu ngày tháng năm thỏa mãn điều kiện của nó (nghĩa là tháng năm phải lớn hơn 0,....)
    if (y > 0 && m > 0 && m < 13 && d > 0 && d <= (songaytrongthang(m, y))) {
        nextday = d + 1;
        //nếu tháng nhập vào không phải tháng 12 và số ngày bằng số ngày tối đa của tháng thì ta tăng tháng lên 1 và ngày = 1
        if (m != 12 && d == songaytrongthang(m, y)) {
            nextday = 1;
            nextmonth = m + 1;
        }
        //nếu tháng nhập vào là tháng 12 và số ngày bằng số ngày bằng 31 thì ta tăng tháng, năm lên 1 và ngày sẽ bằng 1
        else if (m == 12 && d == songaytrongthang(m, y)) {
            nextday = 1;
            nextyear = y + 1;
            nextmonth = 1;
        }
        else if (m == 2) {
            //nếu tháng nhập vào là tháng 2 và năm nhuận thì ngày tối đa sẽ là 29
            if (nhuan(y)) {
                //nếu người dùng nhập vào ngày 29 thì ta tăng tháng lên 1 và ngày bằng 1
                if (d == 29) {
                    nextday = 1;
                    nextmonth = m + 1;
                }
            }
            //ngược lại nếu tháng 2 và không phải năm nhuận thì tháng 2 có 28 ngày
            else {
                //nếu người dùng nhập vào ngày 28 thì tăng tháng lên 1 và ngày bằng 1
                if (d == 28) {
                    nextday = 1; nextmonth = m + 1;
                }
            }
        }
    }
    document.getElementById('kq-date').innerHTML = nextday + "-" + nextmonth + "-" + nextyear;
}
document.getElementById('date-after').onclick = function () {

    var d = document.getElementById('txt-date-inf').value * 1;
    var m = document.getElementById('txt-month-inf').value * 1;
    var y = document.getElementById('txt-year-inf').value * 1;


    var lastyear = y;
    var lastmonth = m;
    var lastday = d;
    //nếu ngày tháng năm thỏa mãn điều kiện của nó (nghĩa là tháng năm phải lớn hơn 0,....)
    if (y > 0 && m > 0 && m < 13 && d > 0 && d <= (songaytrongthang(m, y))) {
        lastday = d - 1;
        //nếu tháng nhập vào != 1 và ngày nhập vào = 1 thì có các trường hợp xảy ra như sau:
        if (m != 1 && d == 1) {
            //trường hơp 1: tháng nhập vào là 2,4,6,8,9,11 thì ngày trước đó sẽ là ngày 31 tháng trước đó
            if (m == 2 || m == 4 || m == 6 || m == 8 || m == 9 || m == 11) {
                lastday = 31;
                lastmonth = m - 1;
            }
            //trường hơp 2: tháng nhập vào là tháng 3 thì ngày trước đó là ngày 29 nếu năm nhuận và 28 nếu không nhuận
            if (m == 3) {
                if (nhuan(y)) {
                    lastday = 29;
                    lastmonth = m - 1;
                }
                else {
                    lastday = 28;
                    lastmonth = m - 1;
                }
            }
            //trường hơp 3: tháng nhập vào là tháng 5,7,10,12 thì ngày trước đó sẽ là 30
            if (m == 5 || m == 7 || m == 10 || m == 12) {
                lastday = 30;
                lastmonth = m - 1;
            }
        }
        //nếu tháng nhập vào là tháng 1 và ngày 1 thì ngày tháng năm trước đó sẽ là ngày 31 tháng 12 năm trước đó
        else if (m == 1 && d == 1) {
            lastday = 31;
            lastyear = y - 1;
            lastmonth = 12;
        }
    }
    document.getElementById('kq-date').innerHTML = lastday + "-" + lastmonth + "-" + lastyear;
}

// câu6
document.getElementById('doc-so').onclick = function () {
    var num = document.getElementById('txt-edge').value;
    var tram = Math.floor(num / 100);
    var chuc = Math.floor((num % 100) / 10);
    console.log("chuc=", chuc);
    var donvi = num % 10;
    var docchuc;
    var docdv;
    var doctram
    switch (tram) {
        case 1:
            doctram = "Một trăm ";
            break;
        case 2:
            doctram = "Hai trăm ";
            break;
        case 3:
            doctram = "Ba trăm ";
            break;
        case 4:
            doctram = "Bốn trăm ";
            break;
        case 5:
            doctram = "Năm trăm ";
            break;
        case 6:
            doctram = "Sáu trăm";
            break;
        case 7:
            doctram = "Bảy trăm";
            break;
        case 8:
            doctram = "Tám trăm ";
            break;
        case 9:
            doctram = "Chín trăm ";
            break;
    }
    if (chuc == 0 && donvi == 0) {
        docchuc = "";
        docdv = "";
    }
    if (chuc == 0 && donvi != 0) {
        docchuc = "lẻ ";
    }
    switch (chuc) {
        case 1:
            docchuc = "mười ";
            break;
        case 2:
            docchuc = "hai mươi ";
            break;
        case 3:
            docchuc = "ba mươi ";
            break;
        case 4:
            docchuc = "bốn mươi ";
            break;
        case 5:
            docchuc = "năm mươi ";
            break;
        case 6:
            docchuc = "sáu mươi ";
            break;
        case 7:
            docchuc = "bảy mươi ";
            break;
        case 8:
            docchuc = "tám mươi ";
            break;
        case 9:
            docchuc = "chín mươi ";
            break;
    }
    switch (donvi) {
        case 1:
            docdv = "một";
            break;
        case 2:
            docdv = "hai";
            break;
        case 3:
            docdv = "ba";
            break;
        case 4:
            docdv = "bốn";
            break;
        case 5:
            docdv = "năm";
            break;
        case 6:
            docdv = "sáu";
            break;
        case 7:
            docdv = "bảy";
            break;
        case 8:
            docdv = "tám";
            break;
        case 9:
            docdv = "chín";
            break;
    }
    document.getElementById('kq-doc-so').innerHTML = doctram + docchuc + docdv;

}
// câu7
document.getElementById('coordinates').onclick = function () {
    var txt_people_1 = document.getElementById('txt-people-1').value;
    var txt_people_2 = document.getElementById('txt-people-2').value;
    var txt_people_3 = document.getElementById('txt-people-3').value;
    var coordinates_people_x = document.getElementById('txt-coordinates-x').value * 1;
    var coordinates_people_y = document.getElementById('txt-coordinates-y').value * 1;

    var coordinates_people_1_x = document.getElementById('txt-people-coordinates-x-1').value * 1;
    var coordinates_people_1_y = document.getElementById('txt-people-coordinates-y-1').value * 1;
    var distance_coordinates_people_1 = Math.floor(Math.sqrt(Math.pow(coordinates_people_1_x - coordinates_people_x, 2) + Math.pow(coordinates_people_1_y - coordinates_people_y, 2)));

    console.log("distance_coordinates_people_1", distance_coordinates_people_1);

    var coordinates_people_2_x = document.getElementById('txt-people-coordinates-x-2').value * 1;
    var coordinates_people_2_y = document.getElementById('txt-people-coordinates-y-2').value * 1;
    var distance_coordinates_people_2 = Math.floor(Math.sqrt(Math.pow(coordinates_people_2_x - coordinates_people_x, 2) + Math.pow(coordinates_people_2_y - coordinates_people_y, 2)));

    console.log("distance_coordinates_people_2: ", distance_coordinates_people_2);

    var coordinates_people_3_x = document.getElementById('txt-people-coordinates-x-3').value * 1;
    var coordinates_people_3_y = document.getElementById('txt-people-coordinates-y-3').value * 1;
    var distance_coordinates_people_3 = Math.floor(Math.sqrt(Math.pow(coordinates_people_3_x - coordinates_people_x, 2) + Math.pow(coordinates_people_3_y - coordinates_people_y, 2)));

    console.log("distance_coordinates_people_3", distance_coordinates_people_3);

    if ((distance_coordinates_people_1 > distance_coordinates_people_2) && (distance_coordinates_people_1 > distance_coordinates_people_3))
        document.getElementById('kq-distance').innerHTML = "Sinh vien xa truong nhat:" + txt_people_1;
    else if ((distance_coordinates_people_2 > distance_coordinates_people_3) && (distance_coordinates_people_2 > distance_coordinates_people_1))
        document.getElementById('kq-distance').innerHTML = "Sinh vien xa truong nhat:" + txt_people_2;
    else
        document.getElementById('kq-distance').innerHTML = "Sinh vien xa truong nhat:" + txt_people_3;
}
// câu8
document.getElementById('date').onclick = function () {
    var month = document.getElementById('txt-month').value * 1;
    var year = document.getElementById('txt-year').value * 1;
    var date;
    switch (month) {
        case 2:
            if ((year % 4 == 0) && (year % 100 != 0) || year % 400 == 0)
                date = 29;
            else
                date = 28;
            break;
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            date = 31;
            break;
        default:
            date = 30;
    }
    document.getElementById('kq-date').innerHTML = "tháng " + month + " năm " + year + " có " + date + " ngày";
}
